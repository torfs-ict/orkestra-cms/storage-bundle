<?php


namespace Orkestra\Storage\Form;

use Doctrine\Common\Annotations\Reader;
use Doctrine\ORM\EntityManagerInterface;
use Orkestra\Storage\Annotation\Thumbnail;
use Orkestra\Storage\Entity\Image;
use Orkestra\Storage\Form\EventListener\ImageTypeListener;
use ReflectionProperty;
use RuntimeException;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

class ImageType extends AbstractType
{
    /**
     * @var Reader
     */
    private $reader;
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var TranslatorInterface
     */
    private $translator;
    /**
     * @var ImageTypeListener
     */
    private $listener;

    public function __construct(Reader $reader, EntityManagerInterface $em, TranslatorInterface $translator, ImageTypeListener $listener)
    {
        $this->reader = $reader;
        $this->em = $em;
        $this->translator = $translator;
        $this->listener = $listener;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class' => Image::class,
                'dropzone_label' => $this->translator->trans('Drop image here')
            ])
            ->setRequired('data_class')
            ->setAllowedTypes('dropzone_label', 'string')
        ;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventSubscriber($this->listener);
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        // Get the owning class and property
        /** @var Image $data */
        $data = $form->getData();
        $data->getOwner($class, $property);

        // Get the Thumbnail annotation
        if (!is_a($options['data_class'], Image::class, true)) {
            throw new RuntimeException(sprintf(
                'ImageType field requires a data_class extending %s.',
                Image::class
            ));
        }
        $thumbnail = $this->reader->getPropertyAnnotation(new ReflectionProperty($class, $property), Thumbnail::class);
        if (!$thumbnail instanceof Thumbnail) $thumbnail = new Thumbnail();
        $view->vars['thumbnail'] = $thumbnail;

        // Calculate the display size
        $height = 200;
        $view->vars['width'] = (int)round($height / $thumbnail->getRatioHeight() * $thumbnail->getRatioWidth());
        $view->vars['height'] = $height;

        // Dropzone label
        $view->vars['dropzone_label'] = $options['dropzone_label'];
    }

    public function getBlockPrefix()
    {
        return 'orkestra_storage_image';
    }
}