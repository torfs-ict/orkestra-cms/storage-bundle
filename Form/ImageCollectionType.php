<?php


namespace Orkestra\Storage\Form;

use Doctrine\Common\Annotations\Reader;
use Orkestra\Storage\Annotation\Thumbnail;
use Orkestra\Storage\Form\EventListener\ImageCollectionTypeListener;
use ReflectionProperty;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

class ImageCollectionType extends AbstractType
{
    /**
     * @var TranslatorInterface
     */
    private $translator;
    /**
     * @var ImageCollectionTypeListener
     */
    private $listener;
    /**
     * @var Reader
     */
    private $reader;

    public function __construct(TranslatorInterface $translator, ImageCollectionTypeListener $listener, Reader $reader)
    {
        $this->translator = $translator;
        $this->listener = $listener;
        $this->reader = $reader;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'dropzone_label' => $this->translator->trans('Drop image here'),
                'add_images_label' => $this->translator->trans('Add new image(s)'),
                'image_class' => null
            ])
            ->setAllowedTypes('dropzone_label', 'string')
            ->setAllowedTypes('image_class', 'string')
        ;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventSubscriber($this->listener);
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        // Get the owning class and property
        $class = $form->getParent()->getConfig()->getDataClass();
        $property = $form->getName();

        // Get the Thumbnail annotation
        $thumbnail = $this->reader->getPropertyAnnotation(new ReflectionProperty($class, $property), Thumbnail::class);
        if (!$thumbnail instanceof Thumbnail) $thumbnail = new Thumbnail();
        $view->vars['thumbnail'] = $thumbnail;

        // Calculate the display size
        $height = 200;
        $view->vars['width'] = (int)round($height / $thumbnail->getRatioHeight() * $thumbnail->getRatioWidth());
        $view->vars['height'] = $height;

        // Dropzone label(s)
        $view->vars['dropzone_label'] = $options['dropzone_label'];
        $view->vars['add_images_label'] = $options['add_images_label'];
    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['multipart'] = true;
    }

    public function getBlockPrefix()
    {
        return 'orkestra_storage_image_collection';
    }
}