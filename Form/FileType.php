<?php

namespace Orkestra\Storage\Form;

use Orkestra\Storage\Entity\AbstractFile;
use Orkestra\Storage\Entity\PrivateFile;
use Orkestra\Storage\StorageService;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\FileType as BaseFileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\PropertyAccess\PropertyAccess;

class FileType extends BaseFileType
{
    /**
     * @var StorageService
     */
    private $storage;

    public function __construct(StorageService $storage)
    {
        $this->storage = $storage;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults([
            'allow_download' => true,
            'download_route' => null,
            'mapped' => false,
            'required' => false
        ]);
        $resolver->addAllowedTypes('allow_download', 'bool');
        $resolver->addAllowedTypes('download_route', ['string', 'null']);
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        // Convert to a Symfony file instance for our view
        $builder->addModelTransformer(new CallbackTransformer(function($file) {
            if ($file instanceof AbstractFile && $file->hasFile()) {
                return new File($this->storage->getRealPath($file));
            } else {
                return null;
            }
        }, function($file) {
            return $file;
        }));

        // Make sure that the current file doesn't get overwritten with NULL when no file is uploaded
        $builder->addEventListener(FormEvents::PRE_SUBMIT, function(FormEvent $event) use ($builder) {
            $data = $event->getData();
            if ($data === null) {
                $event->setData($event->getForm()->getData());
            }
        });

        // As we've set mapped to FALSE we need to manually retrieve the file from the parent object
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function(FormEvent $event) use ($builder) {
            $accessor = PropertyAccess::createPropertyAccessor();
            $object = $event->getForm()->getParent()->getData();
            $propertyPath = $builder->getPropertyPath() ?? $builder->getName();
            $file = $accessor->getValue($object, $propertyPath);

            $event->setData($file);
        });

        // Same as above, but after submitting we need to upload to the parent object
        $builder->addEventListener(FormEvents::POST_SUBMIT, function(FormEvent $event) use ($builder) {
            $upload = $event->getData();
            if (!$upload instanceof UploadedFile) return;
            if (!$upload->isValid()) return;

            $accessor = PropertyAccess::createPropertyAccessor();
            $object = $event->getForm()->getParent()->getData();
            $propertyPath = $builder->getPropertyPath() ?? $builder->getName();
            $file = $accessor->getValue($object, $propertyPath);

            $this->storage->uploadFromFile($file, $upload->getRealPath(), $upload->getClientOriginalName());
        });
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        parent::buildView($view, $form, $options);
        $data = $form->getData();

        // Make sure to set required to FALSE, otherwise we always need to select and upload a file
        if (array_key_exists('required', $view->vars) && $view->vars['required'] === true) {
            $view->vars['required'] = false;
        }

        // Convert an uploaded file to the actual file instance
        if ($data instanceof UploadedFile) {
            $accessor = PropertyAccess::createPropertyAccessor();
            $object = $form->getParent()->getData();
            $propertyPath = $form->getPropertyPath() ?? $form->getName();
            $data = $accessor->getValue($object, $propertyPath);
        }

        // Private files require a custom URL to download
        if ($data instanceof PrivateFile && empty($options['download_route'])) {
            $options['allow_download'] = false;
        }

        // Set the view variables
        $view->vars['allow_download'] = $options['allow_download'];
        $view->vars['download_route'] = $options['download_route'];
        $view->vars['file'] = $data;
    }

    public function getBlockPrefix()
    {
        return 'orkestra_storage_file';
    }
}