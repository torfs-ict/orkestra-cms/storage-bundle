<?php

namespace Orkestra\Storage\Form\EventListener;

use Doctrine\ORM\EntityManagerInterface;
use Orkestra\Storage\Entity\Image;
use Orkestra\Storage\StorageService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class ImageTypeListener implements EventSubscriberInterface
{
    /**
     * @var array
     */
    private $fields = [];
    /**
     * @var array
     */
    private $remove = [];
    /**
     * @var StorageService
     */
    private $storage;
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(StorageService $storage, EntityManagerInterface $em)
    {
        $this->storage = $storage;
        $this->em = $em;
    }

    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SUBMIT => 'onPreSubmit',
            FormEvents::SUBMIT => 'onSubmit'
        ];
    }

    public function onPreSubmit(FormEvent $event)
    {
        $data = $event->getData();
        if (!empty($data)) {
            $form = spl_object_hash($event->getForm());
            if ($data == 'remove') {
                $this->remove[$form] = $data;
            } else {
                $this->fields[$form] = $data;
            }
        }
        $event->setData(null);
    }

    public function onSubmit(FormEvent $event)
    {
        $form = spl_object_hash($event->getForm());
        /** @var Image $data */
        $data = $event->getData();

        if (array_key_exists($form, $this->remove)) {
            $this->storage->remove($data);
            $event->setData($data);
        } elseif (array_key_exists($form, $this->fields)) {
            $upload = json_decode($this->fields[$form], true);
            $data
                ->setCropHeight($upload['actions']['crop']['height'])
                ->setCropWidth($upload['actions']['crop']['width'])
                ->setCropX($upload['actions']['crop']['x'])
                ->setCropY($upload['actions']['crop']['y']);
            $event->setData($data);
            $this->storage->uploadFromData($data, file_get_contents($upload['input']['image']), $upload['input']['name']);
        }
    }
}