<?php

namespace Orkestra\Storage\Form\EventListener;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\PersistentCollection;
use Orkestra\Storage\Entity\Image;
use Orkestra\Storage\StorageService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\PropertyAccess\PropertyAccessor;

class ImageCollectionTypeListener implements EventSubscriberInterface
{
    /**
     * @var array
     */
    private $fields = [];
    /**
     * @var array
     */
    private $remove = [];
    /**
     * @var array
     */
    private $add = [];
    /**
     * @var StorageService
     */
    private $storage;
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var PropertyAccessor
     */
    private $accessor;

    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SUBMIT => 'onPreSubmit',
            FormEvents::SUBMIT => 'onSubmit'
        ];
    }

    public function __construct(StorageService $storage, EntityManagerInterface $em)
    {
        $this->storage = $storage;
        $this->em = $em;
        $this->accessor = new PropertyAccessor();
    }

    public function onPreSubmit(FormEvent $event)
    {
        foreach($event->getData() as $index => $data) {
            if (!empty($data)) {
                $form = spl_object_hash($event->getForm());
                if ($index == 'add') {
                    $this->add[$form] = $data;
                } elseif ($data == 'remove') {
                    if (!array_key_exists($form, $this->remove)) $this->remove[$form] = [];
                    $this->remove[$form][$index] = $data;
                } else {
                    if (!array_key_exists($form, $this->fields)) $this->fields[$form] = [];
                    $this->fields[$form][$index] = $data;
                }
            }
        }
        $event->setData(null);
    }

    public function onSubmit(FormEvent $event)
    {
        $form = spl_object_hash($event->getForm());
        /** @var PersistentCollection|Image[] $data */
        $data = $event->getData();

        if (array_key_exists($form, $this->remove)) {
            foreach($this->remove[$form] as $index => $remove) {
                $file = $data->offsetGet($index);
                $this->storage->remove($file);
                $this->em->remove($file);
                $data->remove($index);
            }
        } elseif (array_key_exists($form, $this->fields)) {
            foreach($this->fields[$form] as $index => $upload) {
                $file = $data->offsetGet($index);
                $upload = json_decode($upload, true);
                $this->storage->uploadFromData($file, file_get_contents($upload['input']['image']), $upload['input']['name']);
                $file
                    ->setCropHeight($upload['actions']['crop']['height'])
                    ->setCropWidth($upload['actions']['crop']['width'])
                    ->setCropX($upload['actions']['crop']['x'])
                    ->setCropY($upload['actions']['crop']['y']);
                $data->offsetSet($index, $file);
            }
        }

        if (array_key_exists($form, $this->add)) {
            $owner = $data->getOwner();
            $property = $data->getMapping()['fieldName'];
            $reference = $data->getMapping()['mappedBy'];
            $class = $event->getForm()->getConfig()->getOption('image_class');
            foreach($this->add[$form] as $upload) {
                /** @var UploadedFile $upload */
                /** @var Image $image */
                $image = new $class;
                $image->setOwner($owner, $property);
                $this->accessor->setValue($image, $reference, $owner);
                $this->storage->uploadFromFile($image, $upload->getRealPath(), $upload->getClientOriginalName());
                $data->add($image);
            }
        }

        $event->setData($data);
    }
}