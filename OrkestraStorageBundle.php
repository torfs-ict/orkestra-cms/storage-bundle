<?php

namespace Orkestra\Storage;

use Orkestra\Storage\DependencyInjection\OrkestraStorageExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class OrkestraStorageBundle extends Bundle
{
    public function getContainerExtension()
    {
        return new OrkestraStorageExtension();
    }
}