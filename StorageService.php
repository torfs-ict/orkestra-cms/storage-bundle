<?php

namespace Orkestra\Storage;

use Doctrine\Common\Annotations\Reader;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Intervention\Image\Constraint;
use Intervention\Image\ImageManagerStatic;
use InvalidArgumentException;
use Orkestra\Storage\Annotation\Directory;
use Orkestra\Storage\Annotation\Thumbnail;
use Orkestra\Storage\Entity\AbstractFile;
use Orkestra\Storage\Entity\Image;
use Orkestra\Storage\Entity\PrivateFile;
use Orkestra\Storage\Entity\PublicFile;
use ReflectionProperty;
use Symfony\Component\Filesystem\Exception\InvalidArgumentException as FileSystemInvalidArgumentException;
use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 * Class Storage
 * @package Orkestra\Service
 */
class StorageService
{
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var Reader
     */
    private $annotationReader;
    /**
     * @var string
     */
    private $projectDir;

    /**
     * @return int[]
     */
    public static function getThumbnailWidths(): array
    {
        return [320, 640, 768, 1024, 1366, 1600, 1920, 3840];
    }

    /**
     * Storage constructor.
     * @param Reader $annotationReader
     * @param string $projectDir
     */
    public function __construct(Reader $annotationReader, string $projectDir)
    {
        $this->annotationReader = $annotationReader;
        $this->projectDir = $projectDir;
    }

    public function configure(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @return EntityRepository
     */
    public function getRepository(): EntityRepository
    {
        return $this->em->getRepository(AbstractFile::class);
    }

    /**
     * @return string
     */
    public function getPrivateDirectory(): string
    {
        return sprintf('%s/storage/private', $this->projectDir);
    }

    /**
     * @return string
     */
    public function getPublicDirectory(): string
    {
        return sprintf('%s/public/storage', $this->projectDir);
    }

    /**
     * @param string $directory
     */
    public function ensureDirectoryExistance(string $directory): void
    {
        if (!is_dir($directory)) {
            mkdir($directory, 0755, true);
        }
    }

    /**
     * @param AbstractFile $file
     * @return string
     * @throws \ReflectionException
     */
    private function getDirectoryForFile(AbstractFile $file): string
    {
        // Get the base storage directory
        if ($file instanceof PublicFile) $base = $this->getPublicDirectory();
        elseif ($file instanceof PrivateFile) $base = $this->getPrivateDirectory();
        else {
            throw new InvalidArgumentException(
                sprintf('File types should extend either %s or %s', PrivateFile::class, PublicFile::class)
            );
        }

        $file->getOwner($owner, $property);
        $reflectionProperty = new ReflectionProperty($owner, $property);
        $directory =
            $this->annotationReader->getPropertyAnnotation($reflectionProperty, Directory::class)
            ?: new Directory();
        return sprintf('%s/%s', $base, $directory->getName());
    }

    /**
     * @param AbstractFile $file
     * @param string $data
     * @param string $filename
     * @param bool $copy
     * @return AbstractFile
     * @throws \ReflectionException
     */
    private function upload(AbstractFile $file, string $data, string $filename, bool $copy = false): AbstractFile
    {
        $directory = $this->getDirectoryForFile($file);
        $this->ensureDirectoryExistance($directory);

        // Put the data in storage
        $extension = pathinfo($filename, PATHINFO_EXTENSION);
        $this->remove($file, true);
        $path = $this->tempnam($file, $extension);
        if ($copy) copy($data, $path);
        else file_put_contents($path, $data);

        // Set metadata for the file
        $src = new File($path);
        $file
            ->setPath(basename($path))
            ->setFilename($filename)
            ->setSize($src->getSize())
            ->setMimeType($src->getMimeType());
        if ($file instanceof Image) $this->generateThumbnails($file);
        return $file;
    }

    /**
     * @param AbstractFile $file
     * @param string $sourcePath
     * @param null|string $filename
     * @return AbstractFile|Image
     * @throws \ReflectionException
     */
    public function uploadFromFile(AbstractFile $file, string $sourcePath, ?string $filename = null): AbstractFile
    {
        if (is_null($filename)) $filename = basename($sourcePath);
        return $this->upload($file, $sourcePath, $filename, true);
    }

    /**
     * @param AbstractFile $file
     * @param string $data
     * @param string $filename
     * @return AbstractFile|Image
     * @throws \ReflectionException
     */
    public function uploadFromData(AbstractFile $file, string $data, string $filename): AbstractFile
    {
        return $this->upload($file, $data, $filename, false);
    }

    /**
     * @param AbstractFile $file
     * @return null|string
     * @throws \ReflectionException
     */
    public function getRealPath(AbstractFile $file): ?string
    {
        if ($file->hasFile()) {
            $directory = $this->getDirectoryForFile($file);
            return sprintf('%s/%s', $directory, $file->getPath());
        } else {
            return null;
        }
    }

    /**
     * @param AbstractFile $file
     * @param bool $keepCropConfiguration
     * @return AbstractFile
     * @throws \ReflectionException
     */
    public function remove(AbstractFile $file, bool $keepCropConfiguration = false): AbstractFile
    {
        $path = $this->getRealPath($file);
        if (is_file($path)) unlink($path);
        if ($file instanceof Image) {
            if (!$keepCropConfiguration) {
                $file
                    ->setCropHeight(null)
                    ->setCropWidth(null)
                    ->setCropX(null)
                    ->setCropY(null);
            }
            foreach([0, 320, 640, 768, 1024, 1366, 1600, 1920, 3840] as $width) {
                try {
                    $path = $this->getThumbnailPath($file, $width);
                    if (is_file($path)) unlink($path);
                } catch (\Throwable $e) {
                }
            }
        }
        return $file
            ->setMimeType(null)
            ->setSize(null)
            ->setFilename(null)
            ->setPath(null);
    }

    /**
     * @param Image $image
     * @param int $width
     * @param bool $relative
     * @return string
     * @throws \ReflectionException
     */
    public function getThumbnailPath(Image $image, int $width = 0, bool $relative = false): string
    {
        if ($relative) {
            $ret = $this->getRelativePath($image);
        } else {
            $ret = $this->getRealPath($image);
        }
        $ret = explode('.', $ret);
        if (count($ret) < 2) throw new FileSystemInvalidArgumentException('Unable to extract file extension');
        $extension = array_pop($ret);
        array_push($ret, $width, $extension);
        return implode('.', $ret);
    }

    /**
     * @param Image $image
     * @return Image
     */
    public function generateThumbnails(Image $image): Image
    {
        if (!$image->hasFile()) return $image;

        // Check for the thumbnail annotation
        $image->getOwner($owner, $property);
        $reflectionProperty = new ReflectionProperty($owner, $property);
        $annotation = $this->annotationReader->getPropertyAnnotation($reflectionProperty, Thumbnail::class);
        if (!$annotation instanceof Thumbnail) $annotation = new Thumbnail();

        // Crop the original image first
        $path = $this->getRealPath($image);
        $default = $this->getThumbnailPath($image);
        $crop = ImageManagerStatic::make($path);
        if (!$image->isCropped()) {
            // Calculate cropping area of the center of the image
            $ratio = $annotation->getRatioWidth() / $annotation->getRatioHeight();
            if ($crop->getHeight() * $ratio > $crop->getWidth()) {
                $height = $crop->getWidth() / $ratio;
                $width = $height * $ratio;
            } else {
                $width = $crop->getHeight() * $ratio;
                $height = $width / $ratio;
            }
            $x = ($crop->getWidth() - $width) / 2;
            $y = ($crop->getHeight() - $height) / 2;
            $crop->crop((int)$width, (int)$height, (int)$x, (int)$y);
            $image
                ->setCropWidth($width)
                ->setCropHeight($height)
                ->setCropX($x)
                ->setCropY($y)
                ->setDefaultWidth($width);
        } else {
            $crop->crop(
                (int)$image->getCropWidth(), (int)$image->getCropHeight(),
                (int)$image->getCropX(), (int)$image->getCropY()
            );
            $image->setDefaultWidth((int)$image->getCropWidth());
        }
        $crop->save($default);

        // Create thumbnails for all the following widths
        foreach(self::getThumbnailWidths() as $width) {
            $thumbnail = $this->getThumbnailPath($image, $width);
            $resize = ImageManagerStatic::make($default);
            if ($resize->width() < $width) continue;
            $resize->resize($width, null, function(Constraint $constraint) {
                $constraint->aspectRatio();
            })->save($thumbnail);
        }

        return $image;
    }

    /**
     * @param AbstractFile $file
     * @param string $extension
     * @return string
     * @throws \ReflectionException
     */
    public function tempnam(AbstractFile $file, string $extension): string
    {
        $directory = $this->getDirectoryForFile($file);
        $this->ensureDirectoryExistance($directory);
        $attempts = 238328; // 62 x 62 x 62
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $length = strlen($chars) - 1;
        $template = sprintf('%s/xxxxxx.%s', $directory, $extension);

        for ($count = 0; $count < $attempts; ++$count) {
            $random = "";

            for($p = 0; $p < 6; $p++) {
                $random .= $chars[mt_rand(0, $length)];
            }

            $randomFile = str_replace('xxxxxx', $random, $template);

            if (!($fd = @fopen($randomFile, 'x+')) )
                continue;

            fclose($fd);
            return $randomFile;
        }

        throw new IOException('Unable to generate temporary file', 0, null, $template);
    }

    public function getRelativePath(PublicFile $file): string
    {
        if (!$file->hasFile()) return '';
        $file->getOwner($owner, $property);
        $reflectionProperty = new ReflectionProperty($owner, $property);
        $directory =
            $this->annotationReader->getPropertyAnnotation($reflectionProperty, Directory::class)
                ?: new Directory();

        return sprintf('storage/%s/%s', $directory->getName(), $file->getPath());
    }

    /**
     * @param PrivateFile $file
     * @return BinaryFileResponse
     */
    public function downloadFile(PrivateFile $file): BinaryFileResponse
    {
        $response = new BinaryFileResponse($this->getRealPath($file));
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $file->getFilename());
        return $response;
    }
}