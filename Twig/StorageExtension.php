<?php

namespace Orkestra\Storage\Twig;

use Orkestra\Storage\Entity\AbstractFile;
use Orkestra\Storage\Entity\Image;
use Orkestra\Storage\Entity\PrivateFile;
use Orkestra\Storage\Entity\PublicFile;
use Orkestra\Storage\StorageService;
use Symfony\Component\Asset\Packages;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class StorageExtension extends AbstractExtension
{
    /**
     * @var StorageService
     */
    private $storage;
    /**
     * @var Packages
     */
    private $packages;
    /**
     * @var UrlGeneratorInterface
     */
    private $urlGenerator;

    public function __construct(StorageService $storage, Packages $packages, UrlGeneratorInterface $urlGenerator)
    {
        $this->storage = $storage;
        $this->packages = $packages;
        $this->urlGenerator = $urlGenerator;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('file', [$this, 'getPath']),
            new TwigFunction('img', [$this, 'responsiveImage'], ['is_safe' => ['html']])
        ];
    }

    public function getPath(AbstractFile $file)
    {
        if (!$file->hasFile()) return '';

        if ($file instanceof PublicFile) {
            return $this->packages->getUrl($this->storage->getRelativePath($file));
        } elseif ($file instanceof PrivateFile) {
            return $this->urlGenerator->generate('storage.file', [
                'file' => $file->getId(),
                'filename' => $file->getFilename()
            ]);
        }
    }

    public function responsiveImage(Image $image, array $attributes = [])
    {
        if (!$image->hasFile()) return '';
        if (!file_exists($this->storage->getRealPath($image))) return '';
        $src = $this->packages->getUrl($this->storage->getThumbnailPath($image, 0, true));
        $srcset = [];

        foreach(StorageService::getThumbnailWidths() as $width) {
            $abs = $this->storage->getThumbnailPath($image, $width);
            if (!file_exists($abs)) continue;
            $rel = $this->packages->getUrl($this->storage->getThumbnailPath($image, $width, true));
            $srcset[] = sprintf('%s %dw', $rel, $width);
        }
        if (!empty($srcset)) $srcset[] = sprintf('%s %dw', $src, $image->getDefaultWidth());
        $srcset = implode(', ', $srcset);

        $attributes = array_merge($attributes, ['src' => $src, 'srcset' => $srcset]);
        $compiled = join('="%s" ', array_keys($attributes)).'="%s" ';
        $attributes = rtrim(vsprintf($compiled, array_map('htmlspecialchars', array_values($attributes))));
        return sprintf('<img %s />', $attributes);
    }
}