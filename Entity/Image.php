<?php

namespace Orkestra\Storage\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Image
 * @package Orkestra\Entity\Storage
 *
 * @ORM\Entity()
 */
class Image extends PublicFile
{
    /**
     * @var float|null
     * @ORM\Column(type="float", nullable=true)
     */
    protected $cropX;
    /**
     * @var float|null
     * @ORM\Column(type="float", nullable=true)
     */
    protected $cropY;
    /**
     * @var float|null
     * @ORM\Column(type="float", nullable=true)
     */
    protected $cropWidth;
    /**
     * @var float|null
     * @ORM\Column(type="float", nullable=true)
     */
    protected $cropHeight;
    /**
     * @var float|null
     * @ORM\Column(type="float", nullable=true)
     */
    protected $defaultWidth;

    /**
     * @return float|null
     */
    public function getCropX(): ?float
    {
        return $this->cropX;
    }

    /**
     * @param float|null $cropX
     * @return $this
     */
    public function setCropX(?float $cropX): Image
    {
        $this->cropX = $cropX;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getCropY(): ?float
    {
        return $this->cropY;
    }

    /**
     * @param float|null $cropY
     * @return $this
     */
    public function setCropY(?float $cropY): Image
    {
        $this->cropY = $cropY;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getCropWidth(): ?float
    {
        return $this->cropWidth;
    }

    /**
     * @param float|null $cropWidth
     * @return $this
     */
    public function setCropWidth(?float $cropWidth): Image
    {
        $this->cropWidth = $cropWidth;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getCropHeight(): ?float
    {
        return $this->cropHeight;
    }

    /**
     * @param float|null $cropHeight
     * @return $this
     */
    public function setCropHeight(?float $cropHeight): Image
    {
        $this->cropHeight = $cropHeight;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getDefaultWidth(): ?float
    {
        return $this->defaultWidth;
    }

    /**
     * @param float|null $defaultWidth
     * @return $this
     */
    public function setDefaultWidth(?float $defaultWidth): self
    {
        $this->defaultWidth = $defaultWidth;
        return $this;
    }

    /**
     * @return $this
     */
    public function reset(): Image
    {
        return $this
            ->setCropWidth(null)
            ->setCropHeight(null)
            ->setCropX(null)
            ->setCropY(null);
    }

    /**
     * @return bool
     */
    public function isCropped(): bool
    {
        return !is_null($this->cropHeight) && !is_null($this->cropWidth)
            && !is_null($this->cropX) && !is_null($this->cropY);
    }
}