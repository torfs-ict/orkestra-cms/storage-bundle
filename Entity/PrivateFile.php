<?php

namespace Orkestra\Storage\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class File
 * @package Orkestra\Entity\Storage
 *
 * @ORM\Entity()
 */
class PrivateFile extends AbstractFile
{
}