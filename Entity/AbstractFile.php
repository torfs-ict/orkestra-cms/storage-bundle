<?php

namespace Orkestra\Storage\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class File
 * @package Orkestra\Entity\File
 *
 * @ORM\MappedSuperclass()
 * @ORM\InheritanceType(value="SINGLE_TABLE")
 * @ORM\Entity()
 * @ORM\Table(name="file")
 */
abstract class AbstractFile
{
    /**
     * @var integer
     * @ORM\Column(type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     */
    protected $filename;
    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     */
    protected $path;
    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     */
    protected $mimeType;
    /**
     * @var int|null
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $size;
    /**
     * @var object
     */
    private $owner;
    /**
     * @var string
     */
    private $ownerProperty;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return null|string
     */
    public function getFilename(): ?string
    {
        return $this->filename;
    }

    /**
     * @param null|string $filename
     * @return $this
     */
    public function setFilename(?string $filename): self
    {
        $this->filename = $filename;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getPath(): ?string
    {
        return $this->path;
    }

    /**
     * @param null|string $path
     * @return $this
     */
    public function setPath(?string $path): self
    {
        $this->path = $path;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getMimeType(): ?string
    {
        return $this->mimeType;
    }

    /**
     * @param null|string $mimeType
     * @return $this
     */
    public function setMimeType(?string $mimeType): self
    {
        $this->mimeType = $mimeType;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getSize(): ?int
    {
        return $this->size;
    }

    /**
     * @param int|null $size
     * @return $this
     */
    public function setSize(?int $size): self
    {
        $this->size = $size;
        return $this;
    }

    /**
     * @return bool
     */
    public function hasFile(): bool
    {
        return !empty($this->path);
    }

    /**
     * @param object $owner
     * @param string $property
     * @return void
     * @internal
     */
    public function getOwner(&$owner, ?string &$property): void
    {
        $owner = $this->owner;
        $property = $this->ownerProperty;
    }

    /**
     * @param object $owner
     * @param string $property
     * @return $this
     * @internal
     */
    public function setOwner(object $owner, string $property): AbstractFile
    {
        $this->owner = $owner;
        $this->ownerProperty = $property;
        return $this;
    }
}