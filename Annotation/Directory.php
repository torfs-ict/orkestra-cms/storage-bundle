<?php

namespace Orkestra\Storage\Annotation;

use Doctrine\Common\Annotations\Annotation\Target;

/**
 * Class Directory
 * @package Orkestra\Annotation\Storage
 *
 * @Annotation
 * @Target("PROPERTY")
 */
class Directory extends AbstractAnnotation
{
    /**
     * The subdirectory used for this kind of file storage.
     *
     * @var string
     */
    private $name = 'default';

    /**
     * Directory constructor.
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        if (isset($data['value'])) {
            $data['name'] = $data['value'];
            unset($data['value']);
        }

        parent::__construct($data);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    protected function setName(string $name): void
    {
        $this->name = $name;
    }
}