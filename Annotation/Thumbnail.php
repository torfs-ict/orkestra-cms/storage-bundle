<?php

namespace Orkestra\Storage\Annotation;

use Doctrine\Common\Annotations\Annotation\Target;

/**
 * Class Thumbnail
 * @package Orkestra\Annotation\Storage
 *
 * @Annotation
 * @Target("PROPERTY")
 */
class Thumbnail extends AbstractAnnotation
{
    /**
     * @var integer
     */
    private $ratioWidth = 4;
    /**
     * @var integer
     */
    private $ratioHeight = 3;
    /**
     * @var int
     */
    private $minWidth = 100;
    /**
     * @var int
     */
    private $minHeight = 100;

    /**
     * @return int
     */
    public function getRatioHeight(): int
    {
        return $this->ratioHeight;
    }

    /**
     * @return int
     */
    public function getRatioWidth(): int
    {
        return $this->ratioWidth;
    }

    /**
     * @return int
     */
    public function getMinHeight(): int
    {
        return $this->minHeight;
    }

    /**
     * @return int
     */
    public function getMinWidth(): int
    {
        return $this->minWidth;
    }

    /**
     * @param int $ratioHeight
     */
    protected function setRatioHeight(int $ratioHeight): void
    {
        $this->ratioHeight = $ratioHeight;
    }

    /**
     * @param int $ratioWidth
     */
    protected function setRatioWidth(int $ratioWidth): void
    {
        $this->ratioWidth = $ratioWidth;
    }

    /**
     * @param int $minHeight
     */
    protected function setMinHeight(int $minHeight): void
    {
        $this->minHeight = $minHeight;
    }

    /**
     * @param int $minWidth
     */
    protected function setMinWidth(int $minWidth): void
    {
        $this->minWidth = $minWidth;
    }
}