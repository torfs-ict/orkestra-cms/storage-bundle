<?php

namespace Orkestra\Storage\Annotation;

abstract class AbstractAnnotation
{
    /**
     * AbstractAnnotation constructor.
     * @param array $data
     * @param bool $failSilent When TRUE, no exception will be thrown for unknown data, it will simply be ignored.
     */
    public function __construct(array $data = [], bool $failSilent = false)
    {
        foreach ($data as $key => $value) {
            $method = 'set'.str_replace('_', '', $key);
            if (method_exists($this, $method)) {
                $this->$method($value);
            } elseif (!$failSilent) {
                throw new \BadMethodCallException(sprintf('Unknown property "%s" on annotation "%s".', $key, \get_class($this)));
            }
        }
    }
}