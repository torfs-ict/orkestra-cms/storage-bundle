<?php

namespace Orkestra\Storage\EventSubscriber;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use Doctrine\ORM\PersistentCollection;
use Orkestra\Storage\StorageService;
use Orkestra\Storage\Entity\AbstractFile;
use Symfony\Component\PropertyAccess\PropertyAccess;

/**
 * Class StorageSubscriber
 * @package Orkestra\Doctrine\Subscriber\Content
 */
class StorageSubscriber implements EventSubscriber
{
    /**
     * @var StorageService
     */
    private $storage;
    /**
     * @var array
     */
    private $queue = [];

    public function __construct(StorageService $storage)
    {
        $this->storage = $storage;
    }

    public function getSubscribedEvents()
    {
        return [
            Events::postLoad,
            Events::preRemove
        ];
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function postLoad(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        $md = $em->getClassMetadata(get_class($entity));
        foreach($md->getAssociationMappings() as $mapping) {
            $field = $mapping['fieldName'];
            $target = $mapping['targetEntity'];
            if (!is_a($target, AbstractFile::class, true)) continue;

            $accessor = PropertyAccess::createPropertyAccessor();
            $data = $accessor->getValue($entity, $field);
            if ($data instanceof PersistentCollection) {
                foreach($data as $entry) {
                    if (!$entry instanceof AbstractFile) continue;
                    $entry->setOwner($entity, $field);
                }
            } elseif ($data instanceof AbstractFile) {
                $data->setOwner($entity, $field);
            }
        }
    }

    public function preRemove(LifecycleEventArgs $args)
    {
        $file = $args->getEntity();
        if (!$file instanceof AbstractFile) return;

        $this->storage->remove($file);
    }
}