Don't forget to add the Easy Admin form themes:

```yaml
easy_admin:
  design:
    form_theme:
      - '@EasyAdmin/form/bootstrap_4.html.twig'
      - '@OrkestraStorage/form.html.twig'
```
